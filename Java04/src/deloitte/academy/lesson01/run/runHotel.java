package deloitte.academy.lesson01.run;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import deloitte.academy.lesson01.entity.FrequentlyCustomer;
import deloitte.academy.lesson01.entity.NewCustomer;
import deloitte.academy.lesson01.entity.Worker;
import deloitte.academy.lesson01.logic.Functions;

/**
 * 
 * @author fduhalt
 *
 */
public class runHotel {
	
	public static final List<NewCustomer> listaTrabajador = new ArrayList<NewCustomer>();
	public static final NewCustomer client1 = new NewCustomer(1,100);
	public static final FrequentlyCustomer client2 = new FrequentlyCustomer(1,300);
	public static final Worker client3 = new Worker(1,250);
	public static final Boolean[] rooms = {false, false, false, true, true, true};
	
	private static final Logger LOGGER = Logger.getLogger(runHotel.class.getName());
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		LOGGER.info(Functions.checkInRoom(rooms));
		LOGGER.info(Functions.checkOutRoom(rooms, client1));
		LOGGER.info(Functions.checkOutRoomF(rooms, client2));
		LOGGER.info(Functions.checkOutRoomW(rooms, client3));
		
	}

}
