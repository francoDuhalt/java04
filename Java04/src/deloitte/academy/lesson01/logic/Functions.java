package deloitte.academy.lesson01.logic;

import java.util.logging.Level;
import java.util.logging.Logger;

import deloitte.academy.lesson01.entity.FrequentlyCustomer;
import deloitte.academy.lesson01.entity.Hotel;
import deloitte.academy.lesson01.entity.NewCustomer;
import deloitte.academy.lesson01.entity.Worker;

/**
 * Class for check In and check Out
 * @author Franco Duhalt
 *
 */

public class Functions {
	private static final Logger LOGGER = Logger.getLogger(Functions.class.getName());
	/**
	 * 
	 * @param hotel
	 */
	public static void ejecutarPago(Hotel hotel) {
		hotel.cobrar();
	}
	/**
	 * This methods makes a checkIn for a room
	 * @param rooms
	 * @return array
	 */
	
	public static String checkInRoom(Boolean[] rooms) {
		String result ="";
		try {
			for (int i = 0; i < rooms.length; i++){
				if ( rooms[i].equals(true)) {
					rooms[i] = false;
					result = "Se ha hecho un check In.";
					break;
					
				}
				
			}
			
		}catch (Exception e) {
			LOGGER.log(Level.SEVERE, "Exception occur", e);
		}
		return result;
	}
	
	/**
	 * This methods makes a checkOut for a room and a payment for a new costumer
	 * @param rooms
	 * @param customer
	 * @return array
	 */
	
	public static String checkOutRoom(Boolean[] rooms, NewCustomer customer) {
		String result ="";
		try {
			ejecutarPago(customer);
			for (int i = 0; i < rooms.length; i++){
				if ( rooms[i].equals(false)) {
					rooms[i] = true;
					result = "Se ha hecho un check out.";
					break;
				}
				
			}
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "Exception occur", e);
		}
		return result;
	}
	
	/**
	 * This method makes a checkOut for a room and a payment for a frequently customer
	 * @param rooms
	 * @param customer
	 * @return array
	 */
	public static String checkOutRoomF(Boolean[] rooms, FrequentlyCustomer customer) {
		String result ="";
		try {
			ejecutarPago(customer);
			for (int i = 0; i < rooms.length; i++){
				if ( rooms[i].equals(false)) {
					rooms[i] = true;
					result = "Se ha hecho un check out con un descuento de cliente frecuente.";
					break;
				}
				
			}
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "Exception occur", e);
		}
		
		return result;
	}
	
	/**
	 * This method makes makes a checkOut for a room and a payment for a worker
	 * @param rooms
	 * @param customer
	 * @return array
	 */
	public static String checkOutRoomW(Boolean[] rooms, Worker customer) {
		String result ="";
		try {
			ejecutarPago(customer);
			for (int i = 0; i < rooms.length; i++){
				if ( rooms[i].equals(false)) {
					rooms[i] = true;
					result = "Se ha hecho un check out con un descuento de trabajdor.";
					break;
				}
				
			}
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "Exception occur", e);
		}
		return result;
	}
}
