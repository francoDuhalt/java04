package deloitte.academy.lesson01.entity;
/**
 * 
 * @author Franco Duhalt
 *
 */
public enum CustomerType {
	newCustomer(1), frequently(.30), worker(.70);
	
	public double importe;
	
	private CustomerType( double importe) {
		this.importe = importe;
	}

	public double getImporte() {
		return importe;
	}

	public void setImporte(int importe) {
		this.importe = importe;
	}
	
}
