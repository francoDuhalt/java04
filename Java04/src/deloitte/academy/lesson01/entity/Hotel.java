package deloitte.academy.lesson01.entity;
/**
 * Abstract class
 * @author Franco Duhalt
 *
 */
public abstract class Hotel {
	private int room;
	private double importe;
	
	
	
	public Hotel() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Hotel(int room, double importe) {
		super();
		this.room = room;
		this.importe = importe;
	}
	
	public int getRoom() {
		return room;
	}
	public void setRoom(int room) {
		this.room = room;
	}
	public double getImporte() {
		return importe;
	}
	public void setImporte(double importe) {
		this.importe = importe;
	}
	
	
	public abstract double cobrar();
	
	
}
