package deloitte.academy.lesson01.entity;
/**
 * 
 * @author fduhalt
 *
 */
public class Worker extends Hotel{

	public Worker() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Worker(int room, double importe) {
		super(room, importe);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public double cobrar() {
		double total = this.getImporte() * CustomerType.worker.importe;
		return total;
	}
	
	

}
