package deloitte.academy.lesson01.entity;
/**
 * 
 * @author fduhalt
 *
 */
public class FrequentlyCustomer extends Hotel{
	
	public FrequentlyCustomer() {
		super();
		// TODO Auto-generated constructor stub
	}

	public FrequentlyCustomer(int room, double importe) {
		super(room, importe);
		// TODO Auto-generated constructor stub
	}

	@Override
	public double cobrar() {
		double total = this.getImporte() * CustomerType.frequently.importe;
		return total;
	}
	

}
