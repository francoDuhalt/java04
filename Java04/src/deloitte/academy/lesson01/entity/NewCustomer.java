package deloitte.academy.lesson01.entity;
/**
 * 
 * @author fduhalt
 *
 */
public class NewCustomer extends Hotel{
	
	public NewCustomer() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public NewCustomer(int room, double importe) {
		super(room, importe);
		// TODO Auto-generated constructor stub
	}

	@Override
	public double cobrar() {
		double total = this.getImporte() * CustomerType.newCustomer.importe;
		return total;
	}
	
	

}
